# Bike Route Map #

This application is to test the feasibility of utilizing Google Maps API for another project through 
implementing it in a simple web application. The intent is to be able to plan a bicycle route, select an optimal route, 
and then save the route's data to some format like JSON or XML.

### What is this repository for? ###

* A simple web app utilizing Google's Map API
* Testing the ability to save the data of a bicycle route to some other format (JSON, XML)

### How do I get set up? ###

You can test the files locally by cloning the repo and then pulling up the HTML file in your browser

### Who do I talk to? ###

* Repo admin: Kathryn Lovett
* Other community or team contact: Professor Stephen Fickas