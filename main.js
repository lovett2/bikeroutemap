var map;

function initMap() {
	var directionsDisplay = new google.maps.DirectionsRenderer;
  var directionsService = new google.maps.DirectionsService;
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 14,
    center: {lat: 37.77, lng: -122.447}
  });

  directionsDisplay.setMap(map);

  calculateAndDisplayRoute(directionsService, directionsDisplay);
  document.getElementById('search').addEventListener("click", function() {
    calculateAndDisplayRoute(directionsService, directionsDisplay);
  });

  if (navigator.geolocation) {
    var watchID = navigator.geolocation.watchPosition(function(position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };

      var markerLoc = new google.maps.Marker({
        position: pos,
        map: map,
        title: 'Current Location'
      });

      map.setCenter(pos);

    });
  } else {
    // Browser doesn't support Geolocation
    // handleLocationError(false, infoWindow, map.getCenter());
  }
}

function calculateAndDisplayRoute(directionsService, directionsDisplay) {
	var start = document.getElementById('start').value;
	var end = document.getElementById('end').value;
  var signal = new google.maps.LatLng(44.040050, -123.088169);
  var polyLine;
  var request = {
    	origin: start,
    	destination: end,
    	travelMode: 'BICYCLING',
    	provideRouteAlternatives: true
  };
    document.getElementById('directions').value = start;

    directionsService.route(request, function(response, status) {
      if (status == 'OK') {

        var polyline = new google.maps.Polyline({
          path: [],
          strokeColor: '#0000FF',
          strokeWeight: 3
        });
        var bounds = new google.maps.LatLngBounds();


        var legs = response.routes[0].legs;
        for (i = 0; i < legs.length; i++) {
          var steps = legs[i].steps;
          for (j = 0; j < steps.length; j++) {
            console.log(steps[j].instructions);
            if(steps[j].instructions.includes("right")) {
              window.alert("Take a RIGHT turn");
            }
            else if(steps[j].instructions.includes("left")) {
              window.alert("Take a LEFT turn");
            }
            var nextSegment = steps[j].path;
            for (k = 0; k < nextSegment.length; k++) {
              polyline.getPath().push(nextSegment[k]);
              bounds.extend(nextSegment[k]);
            }
          }
        }

    		if(google.maps.geometry.poly.isLocationOnEdge(signal, polyline, .01)) {
    			window.alert('Signal is on edge of this route.');
    			var marker = new google.maps.Marker({
        			position: signal,
        			map: map,
        			title: 'Traffic signal'
      		});
    		}
        else {
          window.alert('Signal is NOT on edge of this route.');
        }

        directionsDisplay.setDirections(response);
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });

    directionsDisplay.setPanel(document.getElementById('directions'));
}